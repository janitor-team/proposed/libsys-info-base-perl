libsys-info-base-perl (0.7807-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 18:32:56 +0000

libsys-info-base-perl (0.7807-3) unstable; urgency=medium

  * Team upload.
  * Add libsys-info-driver-linux-perl to Recommends. (Closes: #943597)
  * Drop two modules from debian/tests/pkg-perl/syntax-skip which now have
    their dependencies in Recommends.
  * Declare compliance with Debian Policy 4.4.1.

 -- gregor herrmann <gregoa@debian.org>  Fri, 01 Nov 2019 14:29:24 +0100

libsys-info-base-perl (0.7807-2) unstable; urgency=medium

  * Team upload.
  * Fix autopkgtests.
    Add debian/tests/pkg-perl/syntax-skip with some modules that fail the
    syntax.t test as they would need additional modules which are not
    available in Debian.

 -- gregor herrmann <gregoa@debian.org>  Fri, 22 Feb 2019 14:00:36 +0100

libsys-info-base-perl (0.7807-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org.

  [ gregor herrmann ]
  * Update URLs from {search,www}.cpan.org to MetaCPAN.
  * Update GitHub URLs to use HTTPS.

  [ Jonas Smedegaard ]
  * Simplify rules
    Stop build-depend on devscripts cdbs.
  * Stop build-depend on dh-buildinfo.
  * Declare compliance with Debian Policy 4.3.0.
  * Enable autopkgtest.
  * Set Rules-Requires-Root: no.
  * Wrap and sort control file.
  * Update git-buildpackage config: Filter any .git* file.
  * Update watch file:
    + Bump to file format 4.
    + Track only MetaCPAN URL.
    + Rewrite usage comment.
    + Use substitution strings.
  * Update copyright info:
    + Use https protocol in Format and Upstream-Contact URLs.
    + Extend coverage of packaging.
  * Drop obsolete lintian override regarding debhelper 9.
  * Tighten lintian overrides regarding License-Reference.
  * Stop build-depend on libmodule-build-perl.
  * Drop patch 1001: Obsoleted by change of upstream build system.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 21 Feb 2019 21:12:09 +0100

libsys-info-base-perl (0.7804-2) unstable; urgency=medium

  * Team upload.
  * Add patch to make the build process work without cwd in @INC.
    Thanks to Niko Tyni for coming up with the patch.
    (Closes: #837264)
  * Update lintian override (new wording of the tag).
  * Use HTTPS for Vcs-Git in debian/control.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Sep 2016 17:37:11 +0200

libsys-info-base-perl (0.7804-1) unstable; urgency=medium

  [ upstream ]
  * New release.

  [ Jonas Smedegaard ]
  * Modernize git-buildpackage config: Avoid git- prefix.
  * Update copyright info:
    + Drop Files section for no longer included convenience code copy.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend copyright of packaging to cover current year.
    + Extend copyright for main upstream author to cover recent years.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.
  * Stop build-depend on libcgi-pm-perl: No longer needed for testsuite.
  * Declare compliance with Debian Policy 3.9.6.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 26 Oct 2015 15:17:54 +0100

libsys-info-base-perl (0.7803-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to use cgit web frontend.

  [ gregor herrmann ]
  * Add build dependency on CGI.pm. Used in t/03-basic.t.
    (Closes: #789418)

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Jun 2015 22:20:53 +0200

libsys-info-base-perl (0.7803-2) unstable; urgency=medium

  * Fix build-depend explicitly on libmodule-build-perl.
  * Fix use canonical Vcs-Git URL.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 02 Jul 2014 18:44:29 +0200

libsys-info-base-perl (0.7803-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#750448.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 03 Jun 2014 15:35:03 +0200
